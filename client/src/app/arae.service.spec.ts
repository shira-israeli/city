import { TestBed } from '@angular/core/testing';

import { AraeService } from './arae.service';

describe('AraeService', () => {
  let service: AraeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AraeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
