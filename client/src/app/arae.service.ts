import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AraeService {

  constructor(private http: HttpClient) { }
  url = "http://localhost:44352/api/Areas/"
  getData(): Observable<any> {
    return this.http.get<any>(this.url+"getData");
  }
  getEzorimByMerchav(code: number)
  {
    return this.http.get<any>(this.url+"getEzorimByMerchav/"+ code);

  }
  getRellavantPeople( yeshuv:any,  merchav:any,  ezor:any){
    return this.http.get<any>(this.url+"getRellavantPeople/"+ yeshuv+'/'+ merchav +'/'+ezor);

  }
}
