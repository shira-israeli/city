import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
// import { AnyCnameRecord } from 'dns';
import Swal from 'sweetalert2';
import { AraeService } from '../arae.service';
import { ezorYeshovMerchav } from '../Model/ezorYeshovMerchav';

@Component({
  selector: 'app-areas-list',
  templateUrl: './areas-list.component.html',
  styleUrls: ['./areas-list.component.css']
})
export class AreasListComponent implements OnInit {
  data: any;
  ezorim: any;
  merchavflag = false;
  yeshuv: ezorYeshovMerchav= new ezorYeshovMerchav("",0);
  merchav:  ezorYeshovMerchav= new ezorYeshovMerchav("",0);
  ezor:  ezorYeshovMerchav= new ezorYeshovMerchav("",0);
  select = false;
  constructor(public araeService: AraeService, private router :Router) { }

  ngOnInit(): void {
    this.araeService.getData().subscribe(res => { this.data = res; console.log(res) })
  }
  getEzorimByMerchav(m: any) {
    this.select = true;
    this.merchav = m
    this.araeService.getEzorimByMerchav(m.code).subscribe(res => { this.ezorim = res; this.merchavflag = true; console.log(res) })
  }
  selectE(e: any) {
    this.select = true;
    this.ezor=e;
  }
  selectY(y: any) {
    this.select = true;
    this.yeshuv=y;
  }
  getRellavantPeople()
  {
    if(this.yeshuv==null||this.ezor==null||this.merchav==null)
    {
      Swal.fire({  
        icon: 'error',  
        title: 'אופסס..',  
        text: 'כנראה שכחת לבחור אזור או ישוב או מרחב',    
      })  
    }
    else{
      console.log(this.yeshuv.code, this.merchav.code, this.ezor.code);
      this.router.navigate(['pepole', this.yeshuv.code, this.merchav.code, this.ezor.code]);
    }
  }

}
