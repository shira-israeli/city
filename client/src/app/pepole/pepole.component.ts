import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AraeService } from '../arae.service';
import { Men } from '../Model/men';

@Component({
  selector: 'app-pepole',
  templateUrl: './pepole.component.html',
  styleUrls: ['./pepole.component.css']
})
export class PepoleComponent implements OnInit {
  yeshuv: any;
  merchav: any;
  ezor: any;
  childFlag=false;
  men:Men[]= new Array();
  child: Men[]= new Array();
  constructor(private araeService: AraeService, private route: ActivatedRoute) {
    this.route.paramMap.subscribe(params => {
      this.yeshuv = params.get('y'); this.merchav = params.get('m'); this.ezor = params.get('e');
    })
   }

  ngOnInit(): void {
this.araeService.getRellavantPeople(this.yeshuv,this.merchav, this.ezor).subscribe(res =>{this.men=res} )
  }
  getChiled(id:number){
    this.childFlag=true;
this.child= this.men.find(m=>m.ID==id)?.Children|| new Array();
  }

}
