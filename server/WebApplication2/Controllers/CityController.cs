﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [RoutePrefix("api/Areas")]
    public class CityController : ApiController
    {
        [Route("getData")]
        [HttpGet]
        public clsData getData()
        {
            try
           {

                clsData data = new clsData();
                data.merchavim = new tableCodeDesc[]{ new tableCodeDesc(1, "מרחב 1"),
                                                      new tableCodeDesc(2, "מרחב 2"),
                                                      new tableCodeDesc(3, "מרחב 3"),
                                                      new tableCodeDesc(4, "מרחב 4"),
                                                      new tableCodeDesc(5, "מרחב 5"),
                                                      new tableCodeDesc(6, "מרחב 6"),
                                                      new tableCodeDesc(7, "מרחב 7"),
                                                      new tableCodeDesc(8, "מרחב 8"),
                                                      new tableCodeDesc(9, "מרחב 9"),
                                                      new tableCodeDesc(10, "מרחב 10")
                                                     };

                data.yeshuvim = new tableCodeDesc[]{  new tableCodeDesc(1, "ישוב 1"),
                                                      new tableCodeDesc(2, "ישוב 2"),
                                                      new tableCodeDesc(3, "ישוב 3"),
                                                      new tableCodeDesc(4, "ישוב 4"),
                                                      new tableCodeDesc(5, "ישוב 5"),
                                                      new tableCodeDesc(6, "ישוב 6"),
                                                      new tableCodeDesc(7, "ישוב 7"),
                                                      new tableCodeDesc(8, "ישוב 8"),
                                                      new tableCodeDesc(9, "ישוב 9"),
                                                      new tableCodeDesc(10, "ישוב 10")
                                                    };




                //

                return data;
            }
            catch (Exception e)
            {

                return null;
            }


        }
        [Route("getEzorimByMerchav/{merchav}")]
        public tableCodeDesc[] getEzorimByMerchav(int merchav)
        {
            try
            {
                clsData data = new clsData();
                data.ezorim = new tableCodeDesc[10];
                for (int i = 0; i < 10; i++)
                {
                    data.ezorim[i] = new tableCodeDesc(int.Parse(merchav.ToString() + i.ToString()), "איזור " + merchav.ToString() + i.ToString());
                }
                return data.ezorim;
            }
            catch (Exception ex)
            {

                return null;
            }


        }
        [Route("getRellavantPeople/{yeshuv}/{merchav}/{ezor}")]
        public Man[] getRellavantPeople(int yeshuv, int merchav, int ezor)
        {
            Man[] arrMen = new Man[10];
            for (int i = 0; i < 10; i++)
            {
                arrMen[i] = new Man
                {
                    ID = int.Parse(i.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                    Address = " כתובת " + int.Parse(i.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                    firstName = " שם פרטי " + int.Parse(i.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                    lastname = " שם משפחה " + int.Parse(i.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString())
                };

                arrMen[i].Children = new Man[5];
                for (int j = 0; j < 5; j++)
                {
                    arrMen[i].Children[j] = new Man
                    {
                        ID = int.Parse(j.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                        Address = " כתובת ילד " + int.Parse(j.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                        firstName = " שם פרטי ילד " + int.Parse(j.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString()),
                        lastname = " שם משפחה ילד " + int.Parse(j.ToString() + yeshuv.ToString() + merchav.ToString() + ezor.ToString())
                    };

                }


            }

            return arrMen;
        }

    }
}
