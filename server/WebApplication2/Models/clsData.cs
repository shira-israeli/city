﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebApplication2.Models
{
    //[DataContract]
    public class clsData
    {
        public clsData()
        {

        }

        private tableCodeDesc[] m_merchavim;
        private tableCodeDesc[] m_ezorim;
        private tableCodeDesc[] m_yeshuvim;

        //[DataMember]
        public tableCodeDesc[] merchavim
        {
            get
            {
                return this.m_merchavim;
            }
            set
            {
                m_merchavim = value;
            }
        }

        //[DataMember]
        public tableCodeDesc[] ezorim
        {
            get
            {
                return this.m_ezorim;
            }
            set
            {
                m_ezorim = value;
            }
        }

        //[DataMember]
        public tableCodeDesc[] yeshuvim
        {
            get
            {
                return this.m_yeshuvim;
            }
            set
            {
                m_yeshuvim = value;
            }
        }
    }
    //[DataContract]
    public class tableCodeDesc
    {
        private int m_code;
        private string m_desc;

        public tableCodeDesc(int code, string desc)
        {
            this.code = code;
            this.desc = desc;
        }

        public tableCodeDesc(string code, string desc)
        {
            this.code = int.Parse(code);
            this.desc = desc;
        }

        //[DataMember]
        public int code
        {
            get
            {
                return this.m_code;
            }
            set
            {
                m_code = value;
            }
        }

        //[DataMember]
        public string desc
        {
            get
            {
                return this.m_desc;
            }
            set
            {
                m_desc = value;
            }
        }

    }

    //[DataContract]
    public class Man
    {
        private int m_ID;
        private string m_firstName;
        private string m_lastname;
        private string m_Address;
        private Man[] m_Children;


        //[DataMember]
        public int ID
        {
            get
            {
                return this.m_ID;
            }
            set
            {
                m_ID = value;
            }
        }

        //[DataMember]
        public string firstName
        {
            get
            {
                return this.m_firstName;
            }
            set
            {
                m_firstName = value;
            }
        }

        //[DataMember]
        public string lastname
        {
            get
            {
                return this.m_lastname;
            }
            set
            {
                m_lastname = value;
            }
        }

        //[DataMember]
        public string Address
        {
            get
            {
                return this.m_Address;
            }
            set
            {
                m_Address = value;
            }
        }

        //[DataMember]
        public Man[] Children
        {
            get
            {
                return this.m_Children;
            }
            set
            {
                m_Children = value;
            }
        }

    }
}